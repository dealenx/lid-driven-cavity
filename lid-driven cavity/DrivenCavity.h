﻿#pragma once

#define _USE_MATH_DEFINES
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <direct.h>

#include <iomanip>
#include <ctime>
#include <sstream>

#include "Hole.h"

using namespace std;

class DrivenCavity
{
public:
	DrivenCavity();
	~DrivenCavity();

	int N; // Размерность по x
	int nPlus; // N + 1
	int M; // Размерность по y
	int mPlus; // M + 1

	double a, b;

	double tau, endTime;
	double kappa;

	double hX;
	double hY;

	double *x;
	double *y;

	double Re = 100.0;

	double uWallSpeed = 2.0;

	double **U; // U результирующее
	double **V; // V результирующее

	double **uNext; // U на n+1 шаге
	double **vNext; // V на n+1 шаге

	double **psi; // Ψ на n шаге
	double **psiNext; // Ψ на n+1 шаге

	double **omega; // ω на n шаге
	double **omegaNext; // ω на n+1 шаге

	double eps;
	double** RPsi;
	double** fPsi;
	double** ArPsi;
	double** AuPsi;

	double** ROmega;
	double** fOmega;
	double** ArOmega;
	double** AuOmega;

	int iterations;

	string beginDate;

	void init();

	void solving();

	void setBoundaryUV(double **localU, double **localV);
	void setBoundaryPsiOmega(double **localPsi, double **localOmega);

	double getNorm(double **A, double **B, int localN, int localM);

	void writeMartix(double **ArrForWrite, int localN, int localM);

	double returnWellNum(double num);

	void initDate();

	void saveTecplotFile(string preName, double timeVal, double **psi, double **U, double **V, int localN, int localM);

	// for omega

	void solveOmega();
	void calcOmega();
	void fillRightPartOmega();
	void OperatorOmega(double** res, double** u);
	void findROmega();
	double findTauForOmega();
	double maxElementForOmega(double** a);
	double scalarOmega(double** a, double** b);

	// for psi

	void solvePsi();
	void calcPsi();
	void fillRightPartPsi();
	void OperatorPsi(double** res, double** u);
	void findRPsi();
	double findTauForPsi();
	double maxElementForPsi(double** a);
	void findUV();
	double scalarPsi(double** a, double** b);
};

