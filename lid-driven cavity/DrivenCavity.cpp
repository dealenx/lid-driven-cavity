﻿#include "DrivenCavity.h"


using namespace std;

DrivenCavity::DrivenCavity()
{
	init();
	solving();
}

DrivenCavity::~DrivenCavity()
{
}


/*
* ============================================
* ИНИЦИАЛИЗАЦИЯ ПОЛЕЙ
* ============================================
*/

void DrivenCavity::init()
{
	initDate();

	a = 0.0;
	b = 1.0;

	N = 15;
	M = 15;

	nPlus = N + 1;
	mPlus = M + 1;

	hX = (b - a) / N;
	hY = (b - a) / M;

	tau = hX / 5.0;
	kappa = tau / pow(hX, 2);

	endTime = tau * 10;

	x = new double[nPlus];
	y = new double[mPlus];


	for (int i = 0; i < nPlus; i++)
		x[i] = a + i * hX;

	for (int j = 0; j < mPlus; j++)
		y[j] = a + j * hY;

	U = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		U[i] = new double[mPlus];

	V = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		V[i] = new double[mPlus];

	uNext = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		uNext[i] = new double[mPlus];

	vNext = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		vNext[i] = new double[mPlus];

	psi = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		psi[i] = new double[mPlus];

	psiNext = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		psiNext[i] = new double[mPlus];

	omega = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		omega[i] = new double[mPlus];

	omegaNext = new double *[nPlus];
	for (int i = 0; i < nPlus; i++)
		omegaNext[i] = new double[mPlus];

	// для omega
	
	fOmega = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		fOmega[i] = new double[mPlus];

	ROmega = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		ROmega[i] = new double[mPlus];

	ArOmega = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		ArOmega[i] = new double[mPlus];

	AuOmega = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		AuOmega[i] = new double[mPlus];


	// для PSI
	fPsi = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		fPsi[i] = new double[mPlus];

	RPsi = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		RPsi[i] = new double[mPlus];

	ArPsi = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		ArPsi[i] = new double[mPlus];

	AuPsi = new double *[mPlus];
	for (int i = 0; i < mPlus; i++)
		AuPsi[i] = new double[mPlus];

	for (int i = 0; i < mPlus; i++) 
	{
		for (int j = 0; j < mPlus; j++) 
		{
			U[i][j] = 0;
			V[i][j] = 0;
			
			psi[i][j] = 0;
			psiNext[i][j] = 0;
			
			omega[i][j] = 0;
			omegaNext[i][j] = 0;
			
			U[i][j] = 0;
			V[i][j] = 0;

			// для omega
			fOmega[i][j] = 0;
			ROmega[i][j] = 0;
			ArOmega[i][j] = 0;
			AuOmega[i][j] = 0;

			// для psi
			fPsi[i][j] = 0 ;
			RPsi[i][j] = 0;
			ArPsi[i][j] = 0;
			AuPsi[i][j] = 0;
		}
	}
}

/*
* ============================================
* ОСНОВНОЙ РАСЧЕТ
* ============================================
*/

void DrivenCavity::solving()
{
	

	for (double time = 0.1; time < endTime; time += tau)
	{
		cout << endl << "time: " << time << endl;

		if (time > 0.1) {
			setBoundaryUV(U, V);
			setBoundaryUV(uNext, vNext);

			setBoundaryPsiOmega(psi, omega);
			setBoundaryPsiOmega(psiNext, omegaNext);
		}
		

		solveOmega();

		solvePsi();

		cout << "U: " << endl;
		writeMartix(U, nPlus, mPlus);
		cout << "V: " << endl;
		writeMartix(V, nPlus, mPlus);
		cout << "omegaNext: " << endl;
		writeMartix(omega, nPlus, mPlus);
		cout << "psiNext: " << endl;
		writeMartix(psi, nPlus, mPlus);

		saveTecplotFile("PSI_UV", time, psi, U, V, nPlus, mPlus);
	
	
	} // end time loop
}

void DrivenCavity::setBoundaryUV(double **localU, double **localV)
{
	for (int i = 0; i < nPlus; i++)
	{
		for (int j = 0; j < nPlus; j++)
		{
			
			localU[0][j] = 0;  //Левая граница
			localU[N][j] = 0;  //Правая граница
			localU[i][0] = 0;  //Нижняя граница
			localU[i][M] = uWallSpeed;  //Верхняя граница  с движением
			localU[N][M] = uWallSpeed;
			localU[0][M] = uWallSpeed;

			localV[0][j] = 0;  //Левая граница
			localV[N][j] = 0;  //Правая граница
			localV[i][0] = 0;  //Нижняя граница 
			localV[i][M] = 0;  //Верхняя граница с движением
		}
	}
}

void DrivenCavity::setBoundaryPsiOmega(double **localPsi, double **localOmega)
{
	for (int i = 0; i < nPlus; i++)
	{
		for (int j = 0; j < nPlus; j++)
		{
			localPsi[0][j] = 0;  //Левая граница
			localPsi[N][j] = 0;  //Правая граница
			localPsi[i][0] = 0;  //Нижняя граница с движением
			localPsi[i][M] = 0;  //Верхняя граница

			
			localOmega[0][j] = (psi[0][j] - psi[1][j])*(2.0 / (hX*hX)) * U[0][j] * (2.0 / hX);  //Левая граница
			localOmega[N][j] = (psi[N][j] - psi[N-1][j])*(2.0 / (hX*hX)) * U[N][j] * (2.0 / hX);  //Правая граница
			localOmega[i][0] = (psi[i][0] - psi[i][1])*(2.0 / (hY*hY)) * U[i][0] * (2.0 / hY);  //Нижняя граница с движением
			localOmega[i][M] = (psi[i][M] - psi[i][M-1])*( 2.0/(hY*hY) ) * U[i][M]*(2.0 / hY);  //Верхняя граница
			
		}
	}
}



/*
* ============================================
* Расчет Omega Матрицы
* ============================================
*/


void DrivenCavity::solveOmega() {

	eps = 0.01;
	//k = 2;
	iterations = 0;

	fillRightPartOmega();
	calcOmega();
}

void DrivenCavity::calcOmega() {
	double Rn = 1;
	double tau;

	OperatorOmega(AuOmega, omega);
	findROmega();

	while (Rn > eps) {
		iterations++;
		cout << "OMEGA SOLVING iterations: " << iterations << "Rn: " << Rn << " , > eps: " << eps << endl;

		cout << endl;

		cout << endl;

		OperatorOmega(ArOmega, ROmega);
		tau = findTauForOmega();

		for (int i = 1; i < mPlus - 1; i++) {
			for (int j = 1; j < mPlus - 1; j++) {
				omegaNext[i][j] = omega[i][j] - tau * ROmega[i][j];
			}
		}

		//omega = omegaNext;

		for (int i = 0; i < nPlus; i++)
		{
			for (int j = 0; j < nPlus; j++)
			{
				omega[i][j] = returnWellNum(omegaNext[i][j]);
			}
		}

		OperatorPsi(AuOmega, omega);
		findROmega();
		Rn = maxElementForOmega(ROmega);
	}
	cout << "OMEGA SOLVING iterations: " << iterations << "Rn: " << Rn << " , > eps: " << eps << endl;
	cout << "OUT FROM LOOP ";

	// Надо написать w^n = w^n+1
}

void DrivenCavity::fillRightPartOmega() {
	for (int i = 0; i < mPlus; i++) {
		for (int j = 0; j < mPlus; j++) {
			fOmega[i][j] = returnWellNum(omega[i][j] / tau);
		}
	}
}

void DrivenCavity::OperatorOmega(double** res, double** u) {
	double a, b, c;
	for (int i = 1; i < mPlus - 1; i++) {
		for (int j = 1; j < mPlus - 1; j++) {
			a = (u[i][j] / tau) + U[i][j] * ((u[i + 1][j] - u[i - 1][j]) / (2 * hX));
			b = V[i][j] * ((u[i][j + 1] - u[i][j - 1]) / (2 * hX));
			c = (-1.0 / Re) * (
				((u[i + 1][j] - 2 * u[i][j] + u[i - 1][j]) / (hX*hX)  )
				+
				((u[i][j + 1] - 2 * u[i][j] + u[i][j - 1]) / (hY*hY)  )
				);

			res[i][j] = a + b + c;
		}
	}

}

void DrivenCavity::findROmega() {
	for (int i = 1; i < mPlus - 1; i++) {
		for (int j = 1; j < mPlus - 1; j++) {
			ROmega[i][j] = AuOmega[i][j] - fOmega[i][j];
		}
	}
}



double DrivenCavity::findTauForOmega() {
	double t1 = scalarOmega(ArOmega, ROmega);
	double t2 = scalarOmega(ArOmega, ArOmega);
	return t1 / t2;
}

double DrivenCavity::maxElementForOmega(double** a) {
	double max = 0.0;

	for (int i = 0; i < mPlus; i++) {
		for (int j = 0; j < mPlus; j++) {
			if (abs(a[i][j]) > max) {
				max = abs(a[i][j]);
			}
		}
	}
	return max;
}



double DrivenCavity::scalarOmega(double** a, double** b) {
	double sum = 0.0;
	for (int i = 0; i < mPlus; i++) {
		for (int j = 0; j < mPlus; j++) {
			//sum += h * h * a[i][j] * b[i][j];
			sum += a[i][j] * b[i][j];
		}
	}
	return sum;
}




/*
* ============================================
* Расчет PSI Матрицы
* ============================================
*/
void DrivenCavity::solvePsi() {

	eps = 0.01;
	//k = 2;
	iterations = 0;

	fillRightPartPsi();
	calcPsi();
}

void DrivenCavity::calcPsi() {
	double Rn = 1;
	double tau;
	/*setPsi(psi);
	setPsi(psiNext);*/
	OperatorPsi(AuPsi, psi);
	findRPsi();

	while (Rn > eps) {
		iterations++;
		cout << "PSI SOLVING. iterations: " << iterations << "Rn: " << Rn << " , > eps: " << eps << endl;

		cout << endl;
		
		cout << endl;

		OperatorPsi(ArPsi, RPsi);
		tau = findTauForPsi();

		for (int i = 1; i < mPlus - 1; i++) {
			for (int j = 1; j < mPlus - 1; j++) {
				psiNext[i][j] = psi[i][j] - tau * RPsi[i][j];
			}
		}

		//psi = psiNext;

		for (int i = 0; i < nPlus; i++)
		{
			for (int j = 0; j < nPlus; j++)
			{
				psi[i][j] = returnWellNum(psiNext[i][j]);
			}
		}

		OperatorPsi(AuPsi, psi);
		findRPsi();
		Rn = maxElementForPsi(RPsi);
	}
	cout << "PSI SOLVING. iterations: " << iterations << "Rn: " << Rn << " , > eps: " << eps << endl;
	cout << "OUT FROM LOOP ";

	findUV();
}

void DrivenCavity::fillRightPartPsi() {
	for (int i = 0; i < mPlus; i++) {
		for (int j = 0; j < mPlus; j++) {
			fPsi[i][j] = (-1.0)* omegaNext[i][j];
		}
	}
}

void DrivenCavity::OperatorPsi(double** res, double** u) {
	double a, b;
	for (int i = 1; i < mPlus - 1; i++) {
		for (int j = 1; j < mPlus - 1; j++) {
			a = (u[i + 1][j] - 2.0 * u[i][j] + u[i - 1][j]) / (hX*hX) ;
			b = (u[i][j + 1] - 2.0 * u[i][j] + u[i][j - 1]) / (hY*hY);
			//c = Math.pow(k, 2) * u[i][j];
			
			res[i][j] = a + b;
		}
	}
}

void DrivenCavity::findRPsi() {
	for (int i = 1; i < mPlus - 1; i++) {
		for (int j = 1; j < mPlus - 1; j++) {
			RPsi[i][j] = returnWellNum (AuPsi[i][j] - fPsi[i][j] ) ;
		}
	}
}



double DrivenCavity::findTauForPsi() {
	double t1 = scalarPsi(ArPsi, RPsi);
	double t2 = scalarPsi(ArPsi, ArPsi);
	return t1 / t2;
}

double DrivenCavity::maxElementForPsi(double** a) {
	double max = 0.0;

	for (int i = 0; i < mPlus; i++) {
		for (int j = 0; j < mPlus; j++) {
			if (abs(a[i][j]) > max) {
				max = abs(a[i][j]);
			}
		}
	}
	return max;
}

void DrivenCavity::findUV() {
	for (int i = 1; i < mPlus - 1; i++) {
		for (int j = 1; j < mPlus - 1; j++) {
			U[i][j] = returnWellNum( (psiNext[i + 1][j] - psiNext[i][j]) / hX ); // Это решалось при Гельмгольце
			
			//U[i][j] = (psiNext[i][j + 1] - psiNext[i][j - 1]) / hY;
		}
	}

	for (int i = 1; i < mPlus - 1; i++) {
		for (int j = 1; j < mPlus - 1; j++) {
			
			V[i][j] = -(psiNext[i][j + 1] - psiNext[i][j]) / hX ; // Это решалось при Гельмгольце
			//V[i][j] = (-1.0)* (psiNext[i + 1][j] - psiNext[i - 1][j]) / hX;
		}
	}
}


double DrivenCavity::scalarPsi(double** a, double** b) {
	double sum = 0.0;
	for (int i = 0; i < mPlus; i++) {
		for (int j = 0; j < mPlus; j++) {
			//sum += h * h * a[i][j] * b[i][j];
			sum += a[i][j] * b[i][j];
		}
	}
	return sum;
}



/*
* ============================================
* Вспомогающие методы
* ============================================
*/


double DrivenCavity::getNorm(double **A, double **B, int localN, int localM) {
	double maxVal = fabs(A[0][0] - B[0][0]);

	for (int i = 0; i < localN; i++)
	{
		for (int j = 0; j < localM; j++)
		{
			if (maxVal < fabs(A[i][j] - B[i][j])) {
				maxVal = fabs(A[i][j] - B[i][j]);
			}
		}
	}

	return maxVal;
}

void  DrivenCavity::writeMartix(double **ArrForWrite, int localN, int localM) {
	for (int i = 0; i < localN; i++)
	{
		for (int j = 0; j < localM; j++)
		{
			cout << fixed << setprecision(2) << ArrForWrite[j][i] << " ";
		} cout << endl;
	}
}

/*
* Проверка на NaN/Inf, если таковое, то возвращает нуль
*/
double DrivenCavity::returnWellNum(double num) {

	/*if (isnan(num)) {
		return 0.0;
	}
	else if (isinf(num)) {
		return 0.0;
	}
	else {
		return num;
	}*/
	return num;
}



/*
* ============================================
* ДЛЯ СОХРАНЕНИЯ В TECPLOT ФАЙЛ
* ============================================
*/

void  DrivenCavity::initDate()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
	auto str = oss.str();

	beginDate = str;
}

void DrivenCavity::saveTecplotFile(string preName, double timeVal, double **psi, double **U, double **V, int localN, int localM)
{
	string fileName = preName + "_" + to_string(timeVal) + ".dat";
	string parentFolder = "./data/";
	string folderPath = parentFolder + beginDate + "/";
	string filePath = folderPath + fileName;

	_mkdir(parentFolder.c_str());
	_mkdir(folderPath.c_str());

	ofstream fout;
	fout.open(filePath);
	fout << "TITLE=\"ADI\"" << endl;
	fout << "VARIABLES=\"X\",\"Y\",\"PSI\",\"U\",\"V\"" << endl;
	fout << "ZONE T=\"" << timeVal << "\", F=POINT" << ", I= " << localN << " , J=" << localM << endl;

	for (int i = 0; i < localN; i++)
	{
		for (int j = 0; j < localM; j++)
		{
			fout << (i * hX) << "\t" << (j * hY) << "\t" << psi[i][j] << "\t" << U[i][j] << "\t" << V[i][j] << endl;
		}
	}

	fout.close();
}
